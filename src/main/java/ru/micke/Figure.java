package ru.micke;

public interface Figure {
    public void draw();
    public void printSquare();
}
