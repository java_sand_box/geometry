package ru.micke;

public class Main {
    public static void main(String[] args) {
	    Figure figure = FigureFactory.inputFigure();
	    figure.printSquare();
	    figure.draw();
    }
}
