package ru.micke;
import java.lang.Math;

public class Circle implements Figure{
    private double _radius;

    public Circle(double radius) {
        _radius = radius;
    }

    @Override
    public void draw() {
        System.out.println(" *** ");
        System.out.println("*****");
        System.out.println(" *** ");
    }

    @Override
    public void printSquare() {
        System.out.printf("Square:%f\n", Math.PI * Math.pow(get_radius(),2.));
    }
    public double get_radius() {
        return _radius;
    }

    public void set_radius(double _radius) {
        this._radius = _radius;
    }
}
