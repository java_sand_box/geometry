package ru.micke;
import  java.util.Scanner;

public class IOStream {
    private static Scanner in = new Scanner(System.in);

    public static void message(String message){
        System.out.println(message);
    }

    public static String inputString(){
        return in.next();
    }

    public static Double inputDouble() {
        Double ret;
        try {
            ret = Double.parseDouble(inputString());
        } catch (NumberFormatException NumberFormatException ) {
            message("Неверный тип данных, повторите ввод");
            return inputDouble();
        }
        return ret;
    }
}
