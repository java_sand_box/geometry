package ru.micke;

public class FigureFactory {
    public static Figure inputFigure() {
        IOStream.message("> Введите тип геометрической фигуры");
        IOStream.message("> Прямоугольник:r");
        IOStream.message("> Квадрат:s");
        IOStream.message("> Треугольник:t");
        IOStream.message("> Круг:с");
        String type = IOStream.inputString();
        if(type.equals("r")) {
            return makeRect();
        } if(type.equals("s")) {
            return makeSquare();
        } if(type.equals("t")) {
            return makeTriangle();
        } if(type.equals("c")) {
            return makeCircle();
        } else {
            IOStream. message("Неверный тип фигуры, повторите ввод");
            return inputFigure();
        }
    }
    protected static Rect makeRect() {
        IOStream.message("Введите ширину");
        Double width = IOStream.inputDouble();
        IOStream.message("Введите высоту");
        Double height = IOStream.inputDouble();
        return new Rect(width,height);
    }
    protected static Square makeSquare() {
        IOStream.message("Введите сторону");
        Double width = IOStream.inputDouble();
        return new Square(width);
    }
    protected static Triangle makeTriangle() {
        IOStream.message("Введите сторону треугольника");
        Double a = IOStream.inputDouble();
        IOStream.message("Введите прилежащую сторону");
        Double b = IOStream.inputDouble();
        IOStream.message("Введите угол между ними в градусах");
        Double angle = IOStream.inputDouble();
        return new Triangle(a,b,angle);
    }
    protected static Circle makeCircle() {
        IOStream.message("Введите сторону");
        Double radius = IOStream.inputDouble();
        return new Circle(radius);
    }
}
