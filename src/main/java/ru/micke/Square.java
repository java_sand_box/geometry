package ru.micke;

public class Square extends Rect{
    public Square(double side) {
        super(side, side);
    }

    public void set_width(double _width) {
        super.set_width(_width);
        super.set_height(_width);
    }

    public void set_height(double _height) {
        super.set_width(_height);
        super.set_height(_height);
    }
}
