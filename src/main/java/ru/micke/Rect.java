package ru.micke;

public class Rect implements Figure {
    private double _width;
    private double _height;

    public Rect(double width, double height) {
        _width = width;
        _height = height;
    }

    @Override
    public void draw() {
        System.out.println("****");
        System.out.println("*  *");
        System.out.println("*  *");
        System.out.println("****");
    }

    @Override
    public void printSquare() {
        System.out.printf("Square:%f\n", _width * _height);
    }

    public double get_width() {
        return _width;
    }

    public void set_width(double _width) {
        this._width = _width;
    }

    public double get_height() {
        return _height;
    }

    public void set_height(double _height) {
        this._height = _height;
    }
}
