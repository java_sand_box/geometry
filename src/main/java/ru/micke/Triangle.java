package ru.micke;
import java.lang.Math;

public class Triangle implements Figure {
    private double _sideA;
    private double _sideB;
    private double _sideC;

    public Triangle(double sideA, double sideB, double angleGrad) {
        double angleRad = (angleGrad <= 180.) ? angleGrad : (angleGrad - 180.0);
        angleRad *= Math.PI / 180.0;
        _sideA = sideA;
        _sideB = sideB;
        double cosA = Math.cos(angleRad);
        if(cosA == Float.NaN || cosA == Float.NEGATIVE_INFINITY || cosA == Float.POSITIVE_INFINITY){
            cosA = Math.cos(angleRad + Float.MIN_VALUE);
        }
        _sideC = Math.sqrt(sideA*sideA + sideB*sideB - 2*sideA*sideB*cosA);
    }

    @Override
    public void draw() {
        System.out.println("  *  ");
        System.out.println(" * * ");
        System.out.println("*****");
    }

    @Override
    public void printSquare() {
        System.out.printf("Square:%f\n", calcHeronFormulaSquare());
    }

    public double calcHeronFormulaSquare(){
        double p = 0.5*(_sideA + _sideB + _sideC);
        return Math.sqrt(p*(p - _sideA)*(p - _sideB)*(p - _sideC));
    }
}
